<?php


interface AbstractFactory
{
    /**
     * @return ProductA
     */
    public function createProductA();

    /**
     * @return ProductB
     */
    public function createProductB();

} 