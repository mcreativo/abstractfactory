<?php


class Factory2
{
    /**
     * @return ProductA
     */
    public function createProductA()
    {
        return new ProductA2();
    }

    /**
     * @return ProductB
     */
    public function createProductB()
    {
        return new ProductB2();
    }
} 