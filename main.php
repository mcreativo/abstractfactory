<?php

function __autoload($class_name)
{
    include $class_name . '.php';
}

$opt = getopt('', array('type:'));
$type = isset($opt['type']) ? ($opt['type'] == 2 ? 2 : 1) : 1;

$factoryClass = 'Factory' . $type;
$factory = new $factoryClass();

include 'Client1.php';
include 'Client2.php';

var_dump($productA);
var_dump($productB);

