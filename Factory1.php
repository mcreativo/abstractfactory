<?php


class Factory1 implements AbstractFactory
{

    /**
     * @return ProductA
     */
    public function createProductA()
    {
        return new ProductA1();
    }

    /**
     * @return ProductB
     */
    public function createProductB()
    {
        return new ProductB1();
    }
}